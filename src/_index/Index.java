package _index;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class Index {

	private static JFrame mainFrame = null;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// TODO: handle exception
		}

		mainFrame = new JFrame();
		Container container = mainFrame.getContentPane();
		container.add(new SwingTipsPanel());
		launch(mainFrame, "Swing-tips Helper", 1024, 768);
	}

	public static void launch(final JFrame f, String title, int w, int h) {
		SwingUtilities.invokeLater(() -> {
			f.setTitle(title);
			f.setSize(w, h);
			f.setLocationRelativeTo(null);
			f.setVisible(true);
			f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			f.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					SwingTipsPanel.frames.forEach((f) -> {
						f.dispose();
					});
					System.exit(0);
				}
			});
		});

	}
}
