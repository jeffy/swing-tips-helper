JAVA-SWING-TIPS: https://github.com/aterai/java-swing-tips

- [github link](https://github.com/aterai/java-swing-tips)
  ![在这里插入图片描述](./captions/github.png)

JAVA-SWING-TIPS:是大牛封装得各种 swing 小 demo, 当你没有思路得时候, 或者想象不到得时候可以参考这个仓库,无依赖纯 jdk 实现.

> 因为仓库历史很悠久了, 少部分代码使用 java-ws jar 包,实现在浏览器中启动 applet 和 jnlp 但是不影响整体功能

---

但是每个 Tip 都是一个 gradle 工程, 想查看 java 运行效果感觉很不方便. 所以把这么多 demo 整合到一个快捷助手中

## swing-tips-helper

- 双击打开查看效果
  ![在这里插入图片描述](./captions/demo.gif)
- 直接输入包名, 选择 invokeAsInner 打开 /invokeAsSingle 两种方式打开
- 增加搜索功能 , 支持大小写搜索,
- 其他可自定义的功能的开关

  ### 那么你就可以使用 small ide 工具来浏览代码, 用 helper 助手来搜索查看效果了

![在这里插入图片描述](./captions/ide.png)

![在这里插入图片描述](./captions/demo2.gif)

### gitee 代码下载地址:

[https://gitee.com/liveBetter/swing-tips-helper.git](https://gitee.com/liveBetter/swing-tips-helper.git)

1.  已经用 exe4j 对 swing-tips.jar 进行了封装 exe, 减少了 java -jar swing-tip.jar 的步骤

    > ps: 如果 SwingTips.exe 不可以打开 ( 因为打包时我选择的自动查找系统注册的 java 环境变量)
    > 程序若不可以使用,  请选择合适的程序制作软件制作为你需要的平台可执行程序, windows 可以选择launch4j 或exe4j

2.  如果你对 swing-tips-helper 的界面不满意 那么找到 src 的\_index 文件夹直接定制即可

3.  swing-tips-helper 助手只是分享, 大概只集成了 80%+ 的 swing-tips 的代码功能, 因为 demo 太多了有些资源路径可能拷贝不到位, 我已经在运行界面详细写了 ps 描述, bug 的话建议你 fork 并自行修复 .
    ![在这里插入图片描述](./captions/tip.png)
